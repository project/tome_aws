<?php

namespace Drupal\tome_aws;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Aws S3 Client service.
 */
class AwsS3Client {

  use DependencySerializationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs an AwsS3Client object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {

    $this->config = $config_factory->get('tome_aws.settings');
  }

  /**
   * Check if the configuration is set.
   */
  public function checkConfiguration() {

    return !empty($this->config->get('aws_s3_client_id')) && !empty($this->config->get('aws_s3_client_secret')) && !empty($this->config->get('aws_s3_bucket_name')) && !empty($this->config->get('aws_s3_region'));
  }

  /**
   * Method description.
   */
  public function get() {

    return new S3Client([
      'region'      => $this->config->get('aws_s3_region'),
      'version'     => '2006-03-01',
      'credentials' => [
        'key'    => $this->config->get('aws_s3_client_id'),
        'secret' => $this->config->get('aws_s3_client_secret'),
      ],
    ]);
  }

  /**
   * Get metadata.
   */
  public function getMetadata($key) {

    try {
      return $this->get()->headObject([
        'Bucket' => $this->config->get('aws_s3_bucket_name'),
        'Key'    => $key,
      ]);
    }
    catch (S3Exception $exception) {
      return NULL;
    }
  }

  /**
   * Get paginator.
   */
  public function getPaginator($name) {

    return $this->get()->getPaginator($name, [
      'Bucket' => $this->config->get('aws_s3_bucket_name'),
    ]);
  }

  /**
   * Upload file.
   */
  public function uploadFile($local_file_path, $directoryPath) {

    // Must begin without slash.
    $prepared_source = ltrim($this->getS3BucketPrefix() . $local_file_path, '/');

    $params = [
      'Bucket' => $this->config->get('aws_s3_bucket_name'),
      'Key'    => $prepared_source,
      'Body'   => fopen($directoryPath . '/' . $local_file_path, 'r'),
      'ACL'    => $this->config->get('aws_s3_acl'),
    ];

    $this->get()->putObject($params);

  }

  /**
   * Set remote redirect.
   */
  public function setRemoteRedirect($source, $destination) {

    // Must begin without slash.
    $prepared_source = ltrim($source, '/');

    $params = [
      'Bucket'                  => $this->config->get('aws_s3_bucket_name'),
      'Key'                     => $this->getS3BucketPrefix() . $prepared_source,
      'ACL'                     => $this->config->get('aws_s3_acl'),
      'WebsiteRedirectLocation' => $destination,
    ];

    $this->get()->putObject($params);

  }

  /**
   * Remove file.
   */
  public function removeFile($local_file_path) {

    // Must begin without slash.
    $prepared_source = ltrim($this->getS3BucketPrefix() . $local_file_path, '/');

    $params = [
      'Bucket' => $this->config->get('aws_s3_bucket_name'),
      'Key'    => $prepared_source,
    ];

    $this->get()->deleteObject($params);

  }

  /**
   * Get remote file path.
   */
  public function getRemoteFilePath($local_file_path, $directoryPath) {

    $file_name = str_replace($directoryPath, '', $local_file_path);
    // Must begin without slash.
    return ltrim($this->getS3BucketPrefix() . $file_name, '/');
  }

  /**
   * Get S3 base URL.
   */
  public function getS3BaseUrl() {

    return 'http://' . $this->config->get('aws_s3_bucket_name') . '.s3-website.' . $this->config->get('aws_s3_region') . '.amazonaws.com';
  }

  /**
   * Get S3 bucket prefix.
   */
  private function getS3BucketPrefix($with_trailing_slash = FALSE) {

    return $with_trailing_slash ? $this->config->get('aws_s3_bucket_prefix') : rtrim($this->config->get('aws_s3_bucket_prefix'), '/');
  }

}
