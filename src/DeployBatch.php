<?php

namespace Drupal\tome_aws;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Drupal\tome_base\PathTrait;
use Drupal\tome_static\StaticGeneratorInterface;

/**
 * A service to wrap batch operations for deploying to AWS.
 */
class DeployBatch {

  use PathTrait;
  use DependencySerializationTrait;

  /**
   * The static generator.
   *
   * @var \Drupal\tome_static\StaticGeneratorInterface
   */
  protected $static;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The remote metadata service.
   *
   * @var \Drupal\tome_aws\RemoteMetadata
   */
  protected $remoteMetadata;

  /**
   * The AWS S3 Client.
   *
   * @var \Drupal\tome_aws\AwsS3Client
   */
  protected $awsS3Client;

  /**
   * DeployBatch constructor.
   *
   * @param \Drupal\tome_static\StaticGeneratorInterface $static
   *   The static generator.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   *   The module handler.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\tome_aws\RemoteMetadata $remote_metadata
   *   The remote metadata service.
   * @param \Drupal\tome_aws\AwsS3Client $aws_s3_client
   *   The AWS S3 Client.
   */
  public function __construct(StaticGeneratorInterface $static, FileSystemInterface $file_system, EntityTypeManager $entity_type_manager, ModuleHandler $module_handler, StateInterface $state, RemoteMetadata $remote_metadata, AwsS3Client $aws_s3_client) {

    $this->static     = $static;
    $this->fileSystem = $file_system;

    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler     = $module_handler;

    $this->state          = $state;
    $this->remoteMetadata = $remote_metadata;
    $this->awsS3Client    = $aws_s3_client;
  }

  /**
   * Determines if the site is configured to properly deploy.
   *
   * @return bool
   *   Whether or not the site is configured to properly deploy.
   */
  public function checkConfiguration() {

    return $this->awsS3Client->checkConfiguration();
  }

  /**
   * Determines if a static build exists.
   *
   * @return bool
   *   Whether or not a static build exists.
   */
  public function checkStaticBuild() {

    return file_exists($this->static->getStaticDirectory());
  }

  /**
   * Gets the generated base URL.
   */
  public function getGeneratedBaseUrl() {

    return $this->state->get(StaticGeneratorInterface::STATE_KEY_URL, 'http://127.0.0.1');
  }

  /**
   * Gets a batch object for deploying to AWS.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The batch builder.
   */
  public function getBatch() {

    $batch_builder = new BatchBuilder();

    // 1. Cache remote metadata
    $this->remoteMetadata->sync();

    // 2. Calculate differences.
    $this->calculateContentsDifferences();
    $this->calculateRedirectsDifferences();

    // 3. Incremental Deploy
    foreach ($this->remoteMetadata->getPending() as $item) {
      switch ($item->type) {
        case 'content':
          $batch_builder->addOperation([$this->awsS3Client, 'uploadFile'], [
            $item->uri,
            $this->static->getStaticDirectory(),
          ]);
          break;

        case 'redirect':
          $batch_builder->addOperation([$this->awsS3Client, 'setRemoteRedirect'], [
            $item->uri,
            $item->destination,
          ]);
          break;
      }
    }

    // 4. Delete obsolete files.
    foreach ($this->remoteMetadata->getObsolete() as $item) {
      $batch_builder->addOperation([$this->awsS3Client, 'removeFile'], [
        $item->uri,
      ]);
    }

    return $batch_builder;

  }

  /**
   * Calculate differences between local and remote contents.
   */
  public function calculateContentsDifferences() {

    foreach ($this->fileSystem->scanDirectory($this->static->getStaticDirectory(), '/.*/') as $file) {
      $relativeFilePath = $this->awsS3Client->getRemoteFilePath($file->uri, $this->static->getStaticDirectory());
      $remoteMetadataObject = $this->remoteMetadata->get($relativeFilePath);
      $localETag = md5_file($file->uri);
      if (!$remoteMetadataObject) {
        $this->remoteMetadata->setNewContent(ltrim($relativeFilePath, '/'));
      }
      else {
        if ($remoteMetadataObject->etag !== $localETag) {
          $this->remoteMetadata->setStatus($relativeFilePath, 'pending');
        }
        else {
          $this->remoteMetadata->setStatus($relativeFilePath, 'updated');
        }
      }
    }

  }

  /**
   * Calculate differences between local and remote redirects.
   */
  public function calculateRedirectsDifferences() {

    $redirects = file($this->static->getStaticDirectory() . '/_redirects');

    foreach ($redirects as $redirect) {
      $redirect_arr = explode(' ', trim($redirect));
      $source = ltrim($redirect_arr[0], '/') . '/index.html';
      $destination = str_replace($this->awsS3Client->getS3BaseUrl(), '', $redirect_arr[1]);
      $this->remoteMetadata->setRedirect($source, $destination);

      $remote_destination_obj = $this->awsS3Client->getMetadata($source);

      if ($remote_destination_obj) {
        $remote_destination = $remote_destination_obj->get('WebsiteRedirectLocation');
        if ($remote_destination === $destination) {
          $this->remoteMetadata->setStatus($source, 'updated');
        }
      }

    }

  }

}
