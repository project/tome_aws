<?php

namespace Drupal\tome_aws;

use Drupal\Core\Database\Connection;

/**
 * Remote Metadata service.
 */
class RemoteMetadata {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The tome_aws.aws_s3_client service.
   *
   * @var \Drupal\tome_aws\AwsS3Client
   */
  protected $awsS3Client;

  /**
   * Constructs a RemoteMetadata object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\tome_aws\AwsS3Client $aws_s3_client
   *   The tome_aws.aws_s3_client service.
   */
  public function __construct(Connection $connection, AwsS3Client $aws_s3_client) {
    $this->connection  = $connection;
    $this->awsS3Client = $aws_s3_client;
  }

  /**
   * Method description.
   */
  public function sync() {

    $this->connection->truncate('tome_aws_remote_metadata')->execute();

    $results = $this->awsS3Client->getPaginator('ListObjects');

    foreach ($results as $result) {
      foreach ($result['Contents'] as $object) {
        $etag = rtrim(ltrim($object['ETag'], '"'), '"');
        $this->setContent($object['Key'], $etag, $object['Size']);
      }
    }

  }

  /**
   * Get pending.
   */
  public function getPending() {
    return $this->connection
      ->select('tome_aws_remote_metadata', 't')
      ->condition('status', 'pending')
      ->fields('t')
      ->execute()
      ->fetchAll();
  }

  /**
   * Get obsolete.
   */
  public function getObsolete() {
    return $this->connection
      ->select('tome_aws_remote_metadata', 't')
      ->condition('status', 'obsolete')
      ->fields('t')
      ->execute()
      ->fetchAll();
  }

  /**
   * Set new content.
   */
  public function setNewContent($uri) {
    $this->connection
      ->merge('tome_aws_remote_metadata')
      ->key('uri', $uri)
      ->fields([
        'uri'  => $uri,
        'type' => 'content',
        'status' => 'pending',
      ])->execute();
  }

  /**
   * Set content.
   */
  public function setContent($uri, $etag, $size) {
    $this->connection
      ->merge('tome_aws_remote_metadata')
      ->key('uri', $uri)
      ->fields([
        'uri'  => $uri,
        'type' => 'content',
        'etag' => $etag,
        'size' => $size,
        'status' => 'obsolete',
      ])->execute();
  }

  /**
   * Set redirect.
   */
  public function setRedirect($uri, $destination) {
    $this->connection
      ->merge('tome_aws_remote_metadata')
      ->key('uri', $uri)
      ->fields([
        'uri'  => $uri,
        'type' => 'redirect',
        'destination' => $destination,
        'status' => 'pending',
      ])->execute();
  }

  /**
   * Set status.
   */
  public function setStatus($uri, $status) {
    $this->connection
      ->update('tome_aws_remote_metadata')
      ->condition('uri', $uri)
      ->fields([
        'status'  => $status,
      ])
      ->execute();
  }

  /**
   * Get connection.
   */
  public function get($uri) {
    return $this->connection
      ->select('tome_aws_remote_metadata', 't')
      ->condition('uri', $uri)
      ->fields('t')
      ->execute()
      ->fetchObject();
  }

}
