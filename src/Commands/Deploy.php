<?php

namespace Drupal\tome_aws\Commands;

use Drupal\tome_aws\DeployBatch;
use Drush\Commands\DrushCommands;

/**
 * Contains the tome-aws:deploy command.
 */
class Deploy extends DrushCommands {

  /**
   * The batch service.
   *
   * @var \Drupal\tome_aws\DeployBatch
   */
  protected $batch;

  /**
   * TomeAwsDeployCommand constructor.
   *
   * @param \Drupal\tome_aws\DeployBatch $batch
   *   The batch service.
   */
  public function __construct(DeployBatch $batch) {
    $this->batch = $batch;
  }

  /**
   * Deploys a static build to AWS.
   *
   * @command tome-aws:deploy
   * @option title A title to identify this build.
   */
  public function deploy(array $options = ['title' => 'Sent from Tome AWS']) {

    if (!$this->batch->checkConfiguration()) {
      $this->io()->error('Tome AWS has not been configured.');
      return 1;
    }
    if (!$this->batch->checkStaticBuild()) {
      $this->io()->error('No static build available for deploy.');
      return 1;
    }
    $batch_builder = $this->batch->getBatch($options['title']);

    batch_set($batch_builder->toArray());
    $result = drush_backend_batch_process();
    if (!is_array($result)) {
      $this->io()
        ->error('Deploy failed - consult the error log for more details.');
      return 1;
    }
    if (!empty($result['object'][0]['errors'])) {
      foreach ($result['object'][0]['errors'] as $error) {
        $this->io()->error($error);
      }
      $this->io()
        ->error('Deploy failed - consult the error log for more details.');
      return 1;
    }
    else {
      /* @todo see https://github.com/drush-ops/drush/issues/5009 */
      if (!empty($result['object'][0]['admin_url']) && !empty($result['object'][0]['deploy_ssl_url'])) {
        $admin_url = $result['object'][0]['admin_url'];
        $deploy_ssl_url = $result['object'][0]['deploy_ssl_url'];
        $this->io()->success(t("Deploy complete!\n View deploy: @DEPLOYURL \nPublish deploy: @ADMINURL/deploys", [
          '@DEPLOYURL' => $deploy_ssl_url,
          '@ADMINURL' => $admin_url,
        ]));
      }
      else {
        $this->io()->success(t("Deploy complete!"));
      }
    }
  }

  /**
   * Batch finished callback after the static site has been deployed.
   *
   * @param bool $success
   *   Whether or not the batch was successful.
   * @param mixed $results
   *   Batch results set with context.
   */
  public static function finishCallback($success, $results) {}

}
