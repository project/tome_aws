<?php

namespace Drupal\tome_aws\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configures Tome AWS settings for the site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'tome_aws_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return ['tome_aws.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form   = parent::buildForm($form, $form_state);
    $config = $this->config('tome_aws.settings');

    $form['aws_s3_client_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('AWS S3 Client ID'),
      '#default_value' => $config->get('aws_s3_client_id'),
    ];

    $form['aws_s3_client_secret'] = [
      '#type'        => 'textfield',
      '#title'       => $this->t('AWS S3 Client Secret'),
      '#placeholder' => !empty($config->get('aws_s3_client_secret')) ? $this->t('A AWS Client Secret has been provided but is not displayed for security reasons.') : '',
    ];

    $form['aws_s3_bucket_name'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('AWS S3 Bucket Name'),
      '#description'   => $this->t('The AWS S3 Bucket Name.'),
      '#default_value' => $config->get('aws_s3_bucket_name'),
    ];

    $form['aws_s3_bucket_prefix'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('AWS S3 Bucket Prefix'),
      '#description'   => $this->t('The AWS S3 Bucket Prefix.'),
      '#default_value' => $config->get('aws_s3_bucket_prefix'),
    ];

    $form['aws_s3_region'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('AWS S3 Region'),
      '#description'   => $this->t('The AWS S3 Region (eg. eu-west-1)'),
      '#default_value' => $config->get('aws_s3_region'),
    ];

    $awsACLOverviewLink = Link::fromTextAndUrl($this->t('AWS Access control list (ACL) overview'),
      Url::fromUri('https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html',
        ['attributes' => ['target' => '_blank']]));

    $form['aws_s3_acl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS S3 ACL'),
      '#description' => $this->t('The AWS S3 Access control list (e.g. public-read). See @link', ['@link' => $awsACLOverviewLink->toString()]),
      '#default_value' => $config->get('aws_s3_acl'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // @todo validate "aws_s3_bucket_prefix" (must has trailing slash)
    parent::submitForm($form, $form_state);

    $config = $this->config('tome_aws.settings');

    $config->set('aws_s3_client_id', $form_state->getValue('aws_s3_client_id'));

    if (!empty($form_state->getValue('aws_s3_client_secret'))) {
      $config->set('aws_s3_client_secret', $form_state->getValue('aws_s3_client_secret'));
    }

    $config->set('aws_s3_bucket_name', $form_state->getValue('aws_s3_bucket_name'));
    $config->set('aws_s3_bucket_prefix', $form_state->getValue('aws_s3_bucket_prefix'));
    $config->set('aws_s3_region', $form_state->getValue('aws_s3_region'));
    $config->set('aws_s3_acl', $form_state->getValue('aws_s3_acl'));

    $config->save();

  }

}
